<?php

class Comments extends Tools_Plugins_Abstract
{
    /**
     * System params
     * @var
     */
    protected $_isAdmin;
    protected $_isUser;
    protected $_isGuest;

    /**
     * @var null
     */
    private $_layout = null;

    /**
     * Const
     */
    const TYPE_PUBLISHED = 1;
    const TYPE_UNPUBLISHED = 0;
    const FOLDER_NAME = 'commentsSystem-userImages';
    const FORM_USERIMAGE_SIZE = 150;
    const COMMENT_USERIMAGE_SIZE = 60;
    const ROLE_MEMBER = 'member';
    const RESOURCE_MEDIA = 'media';


    const SUCCESS_CAPTION = 'Awesome shot!';

    const ERROR_CAPTION = 'Oh no!';

    const INFO_CAPTION = 'Little NOTE!';

    const RESPONSE_TYPE_OK = 1;

    const RESPONSE_TYPE_ERROR = 2;

    const RESPONSE_TYPE_INFO = 3;

    /**
     * Init method.
     *
     */
    protected function _init()
    {
        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(Zend_Layout::getMvcInstance()->getLayoutPath());

        $this->_view = new Zend_View(array('scriptPath' => __DIR__ . '/views'));
        $this->_view->setHelperPath(APPLICATION_PATH . '/views/helpers/');
        $this->_view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        if (($scriptPaths = $this->_layout->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->setScriptPath(dirname(__FILE__) . '/system/views/');

        $this->_isAdmin = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT);
        $this->_isUser  = $this->_sessionHelper->getCurrentUser()->getRoleId(
        ) != Tools_Security_Acl::ROLE_GUEST ? true : false;
        $this->_isGuest = $this->_sessionHelper->getCurrentUser()->getRoleId(
        ) == Tools_Security_Acl::ROLE_GUEST ? true : false;
    }

    /**
     * Run method.
     *
     * @param array $requestedParams
     */
    public function run($requestedParams = array())
    {
        $this->_requestedParams = $requestedParams;
        parent::run($requestedParams);

        $this->_initPlugin();
    }

    /**
     * Init plugin method.
     *
     */
    protected function _initPlugin()
    {

    }

    /**
     * Before controller method.
     *
     */
    public function beforeController()
    {
        $acl = Zend_Registry::get('acl');
        $acl->deny(Tools_Security_Acl::ROLE_MEMBER, self::RESOURCE_MEDIA);
        $acl->allow(self::ROLE_MEMBER, Tools_Security_Acl::RESOURCE_MEDIA);
        Zend_Registry::set('acl', $acl);
    }

    public function afterController()
    {

    }

    /**
     * Add Comment method.
     *
     */
    public function addCommentAction()
    {
        $errors = array();
        if ($this->_request->isPost()) {
            $form = new Comments_Forms_AddComment();

            $noModerate = $this->_request->getParam('noModerate');

            if($this->_request->getParam('captcha') == 'true'){
                $googleRecaptcha = new Tools_System_GoogleRecaptcha();
                if(!$googleRecaptcha->isValid($this->_request->getParam('captchaKey'))){
                    if (!$googleRecaptcha->isValid($this->_request->getParam('captchaKey'))) {
                        $errors[] = array(
                            'responseText' => $this->_translator->translate(
                                "Incorrect recaptcha result"
                            ),
                            'class'        => 'error',
                            'error'        => true,
                        );
                        echo json_encode($errors);
                        return false;
                    }
                }
            }

            //remove excess fields
            if ($this->_isUser) {
                $form->removeElement('email');
                $form->removeElement('fullName');
            }
            if ($form->isValid($this->_request->getPost())) {
                $comment = new Comments_Models_Comment($form->getValues());
                $message = str_replace('$', '&dollar;', strip_tags($this->_request->getParam('message')));

                // Check message
                $spamMapper = Comments_Mappers_Spam::getInstance()->findByMessage($message);
                if ($spamMapper) {
                    $errors[] = array(
                        'responseText' => $this->_translator->translate(
                            "This message has been flagged as spam! Maybe you're not real?"
                        ),
                        'class'        => 'error',
                        'error'        => true,
                    );
                } else {
                    // set data from form
                    if ($this->_isUser) {
                        if ($this->_isUser) {
                            if ($this->_sessionHelper->getCurrentUser()->getFullName()) {
                                $this->_userName = $this->_sessionHelper->getCurrentUser()->getFullName();
                            } else {
                                $this->_userName = $this->_sessionHelper->getCurrentUser()->getRoleId();
                            }
                        }

                        $comment->setEmail($this->_sessionHelper->getCurrentUser()->getEmail());
                        $comment->setFullName($this->_userName);
                        $comment->setUserId($this->_sessionHelper->getCurrentUser()->getId());
                    }

                    if ($this->_isAdmin || $noModerate) {
                        $comment->setPublished(self::TYPE_PUBLISHED);
                    } else {
                        $comment->setPublished(self::TYPE_UNPUBLISHED);
                    }

                    $comment->setSignUp($this->_request->getParam('signUp'));
                    $comment->setMessage($message);
                    $comment->setIpAddress($this->_request->getServer('REMOTE_ADDR'));

                    $trigger = $comment->getReplyId(
                    ) ? Comments_MailWatchdog::TRIGGER_NEW_REPLY : Comments_MailWatchdog::TRIGGER_NEW_COMMENT;
                    $comment->registerObserver(new Tools_Mail_Watchdog(array('trigger' => $trigger)));
                    // Save
                    $result = Comments_Mappers_Comment::getInstance()->save($comment);
                    if ($result) {
                        $comment->notifyObservers();
                        if ($this->_isAdmin || $noModerate) {
                            $errors[] = array(
                                'responseText' => $this->_translator->translate(
                                    "Thank you! Your comment has been posted"
                                ),
                                'class'        => 'success',
                                'error'        => false,
                            );
                        } else {
                            $errors[] = array(
                                'responseText' => $this->_translator->translate(
                                    "Thank you! Your comment will appear after moderation"
                                ),
                                'class'        => 'success',
                                'error'        => false,
                            );
                        }
                        if ($comment->getSignUp() && $this->_isGuest) {
                            //$userData = array();
                            $userData = $this->_request->getPost();
                            if (!Application_Model_Mappers_UserMapper::getInstance()->findByEmail($userData['email'])) {
                                $userData['password'] = rand(1000000, 9000000);
                                $user                 = new Application_Model_Models_User((array)$userData);
                                $user->registerObserver(
                                    new Tools_Mail_Watchdog(
                                        array(
                                            'trigger' => Tools_Mail_SystemMailWatchdog::TRIGGER_SIGNUP
                                        )
                                    )
                                );
                                $user->setRoleId(Tools_Security_Acl::ROLE_MEMBER);
                                if (isset($this->_helper->session->refererUrl)) {
                                    $user->setReferer($this->_helper->session->refererUrl);
                                }
                                $signupResult = Application_Model_Mappers_UserMapper::getInstance()->save($user);
                                if (!$user->getId()) {
                                    $user->setId($signupResult);
                                }
                                //send mails by notifying mail observer about successful sign-up,
                                //$user->notifyObservers();
                                $errors[] = array(
                                    'responseText' => $this->_translator->translate("Now you have registered"),
                                    'class'        => 'success',
                                    'error'        => false,
                                );
                            } else {
                                $errors[] = array(
                                    'responseText' => $this->_translator->translate("You are already registered"),
                                    'class'        => 'error',
                                    'error'        => true,
                                );
                            }
                        }
                    }
                }
            } else {
                $errors[] = array(
                    'responseText' => $this->_translator->translate("Something went wrong! Please check data in the fields."),
                    'class'        => 'error',
                    'error'        => true,
                );
                /*$errorMessages = $form->getMessages();
                $errorMessage  = '';
                foreach ($errorMessages as $message) {
                    foreach ($message as $msg) {
                        $errorMessage .= $msg . '</br>';
                        $errors[] = array('responseText' => $msg, 'class' => 'error', 'error' => true);
                    }
                }*/
            }
        }
        echo json_encode($errors);
    }

    public function removeCommentAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS) && $this->_request->isPost()) {
            $commentId     = $this->_request->getParam('id');
            $commentMapper = Comments_Mappers_Comment::getInstance();

            if ($commentMapper->find($commentId)) {
                $comment     = $commentMapper->find($commentId);

                $subComments = $commentMapper->fetchAll(array('replyId = ?' => $commentId));

                if (count($subComments) != 0) {
                    $commentMapper->deleteSubComments($comment);
                    $this->_responseHelper->success($this->_translator->translate('Comment an answers deleted'));
                } else {
                    $commentMapper->delete($comment);
                    $this->_responseHelper->success($this->_translator->translate('Comment deleted'));
                }
            }
        }
    }

    /**
     * Edit comment method
     *
     * @throws Zend_Controller_Action_Exception
     */
    public function editCommentAction()
    {
        if ($this->_request->isPost()) {
            $session   = Zend_Controller_Action_HelperBroker::getExistingHelper('session');
            $validator = new Zend_Validate_NotEmpty();
            if (!$validator->isValid($this->_request->getPost('message'))) {
                exit;
            } else {
                $comment = Comments_Mappers_Comment::getInstance()->find($this->_request->getPost('id'));
                if (!is_null($comment)) {
                    if ($comment->getUserId() == $session->getCurrentUser()->getId()
                        || Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)
                    ) {
                        $comment->setMessage(str_replace('$', '&dollar;', $this->_request->getPost('message')));
                        Comments_Mappers_Comment::getInstance()->save($comment);
                        echo json_encode(
                            array(
                                'responseText' => $this->_translator->translate("Comment saved!"), 'class' => 'success'
                            )
                        );
                    } else {
                        echo json_encode(
                            array(
                                'responseText' => $this->_translator->translate("Something went wrong"),
                                'class'        => 'error'
                            )
                        );
                    }
                }
            }

        }
    }

    /**
     * Publish comment method
     *
     */
    public function publishCommentAction()
    {
        if ($this->_request->getPost() && Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $comment = Comments_Mappers_Comment::getInstance()->find($this->_request->getPost('id'));
            if (!is_null($comment)) {
                $result = Comments_Mappers_Comment::getInstance()->save(
                    $comment->setPublished(self::TYPE_PUBLISHED)
                );
                if ($result) {
                    //$comment->notifyObservers();
                    echo json_encode(
                        array(
                            'responseText' => $this->_translator->translate("Comment published!"),
                            'class'        => 'success',
                            'error'        => false
                        )
                    );

                    return true;
                } else {
                    echo json_encode(array('error' => true));
                }
            }
        }
    }

    /**
     * Move to spam method.
     *
     */
    public function moveToSpamAction()
    {
        if ($this->_request->getPost() && Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $comment = Comments_Mappers_Comment::getInstance()->find($this->_request->getPost('id'));
            if (!is_null($comment)) {
                $comment->setSpam(1);
                $comment->setPublished(0);
                $result = Comments_Mappers_Comment::getInstance()->save($comment);

                // Save to spam table
                $spamModel = new Comments_Models_Spam();
                // clear message
                $message = trim(strtolower($comment->getMessage()));
                $spamModel->setMessage($message);
                $spamModel->setUserId($this->_sessionHelper->getCurrentUser()->getId());
                Comments_Mappers_Spam::getInstance()->save($spamModel);

                if ($result) {
                    echo json_encode(
                        array(
                            'responseText' => $this->_translator->translate("Comment move to spam!"),
                            'class'        => 'success',
                            'error'        => false
                        )
                    );

                    return true;
                } else {
                    echo json_encode(array('error' => true));
                }
            }
        }
    }

    /**
     * Get comments list method
     *
     */
    public function getCommentsListAction()
    {
        if ($this->_request->isPost()) {
            $listName = $this->_request->getParam('listName');
            $listType = $this->_request->getParam('listType');
            $pageId   = $this->_request->getParam('pageId');
            $spam     = 0;
            //$onlyPublished = $this->_isAdmin ? false : true;
            $onlyPublished = false;

            $commentsList = Comments_Mappers_Comment::getInstance()->fetchMultiple(
                $listName,
                $listType,
                $pageId,
                $spam,
                $onlyPublished
            );

            /*foreach($commentsList as $key => $field){
                $commentsList[$key]['datePublished'] = strtotime($commentsList[$key]['datePublished']);
            }*/

            echo json_encode($commentsList);
        }
    }

    /**
     * Change rating method.
     *
     */
    public function changeRatingAction()
    {
        if ($this->_sessionHelper->getCurrentUser()->getRoleId(
            ) != Tools_Security_Acl::ROLE_GUEST && $this->_request->isPost()
        ) {
            $id     = $this->_request->getParam('id');
            $value  = $this->_request->getParam('value');
            $userId = $this->_sessionHelper->getCurrentUser()->getId();

            $ratingMapper = Comments_Mappers_Rating::getInstance();
            if (!$ratingMapper->getRatingPoint($userId, $id)) {
                $commentMapper = Comments_Mappers_Comment::getInstance();
                $comment       = $commentMapper->find($id);
                $comment->setRating($value);
                $commentMapper->save($comment);

                echo json_encode(
                    array(
                        'responseText' => $this->_translator->translate("You have successfully voted"),
                        'class'        => 'success',
                        'error'        => false
                    )
                );

                //rating
                $rating = new Comments_Models_Rating();
                $rating->setCommentId($id);
                $rating->setValue($value);
                $rating->setUserId($userId);
                $ratingMapper->save($rating);
            } else {
                echo json_encode(
                    array(
                        'responseText' => $this->_translator->translate("You have already voted for this comment."),
                        'class'        => 'error',
                        'error'        => true
                    )
                );
            }
        }
    }

    /**
     * Get reply form template method.
     *
     */
    public function getReplyTemplateAction()
    {
        $templateName = 'replyform';
        echo $this->_view->render('js.' . $templateName . '.phtml');
    }

    public function saveImageDataAction()
    {
        if ($this->_sessionHelper->getCurrentUser()->getRoleId() != Tools_Security_Acl::ROLE_GUEST) {
            $imageData   = $this->_request->getParams();
            $imageName   = str_replace(" ", "-", strtolower($imageData['imageName']));
            $uploadModel = new Comments_Models_User();
            $uploadModel
                ->setId($this->_sessionHelper->getCurrentUser()->getId())
                ->setImageName($imageName);
            $pathToDirectory     = $this->_websiteHelper->getPath(
                ) . 'media' . DIRECTORY_SEPARATOR . self::FOLDER_NAME . DIRECTORY_SEPARATOR;
            $thumbnailsDirectory = $pathToDirectory . 'thumbnails' . DIRECTORY_SEPARATOR;
            $cropDirectory       = $pathToDirectory . 'crop' . DIRECTORY_SEPARATOR;

            if (!is_dir($thumbnailsDirectory)) {
                Tools_Filesystem_Tools::mkdir($thumbnailsDirectory);
            }

            $useCrop = true;

            // Delete old img
            $imgTypes = array('jpg' => 'jpg', 'png' => 'png', 'jpeg' => 'jpeg', 'gif' => 'gif');
            $imgType  = explode('.', $imageName);
            $imgType  = end($imgType);
            unset($imgTypes[$imgType]);

            $oldPreviews = Tools_Filesystem_Tools::findFilesByExtension(
                $pathToDirectory . Tools_Image_Tools::FOLDER_ORIGINAL . DIRECTORY_SEPARATOR,
                implode('|', $imgTypes),
                true,
                true,
                true
            );
            if (isset($oldPreviews[$this->_sessionHelper->getCurrentUser()->getFullName()])) {
                Tools_Filesystem_Tools::deleteFile(
                    $oldPreviews[$this->_sessionHelper->getCurrentUser()->getFullName()]
                );
            }

            Tools_Image_Tools::resizeByParameters(
                $pathToDirectory . Tools_Image_Tools::FOLDER_ORIGINAL . DIRECTORY_SEPARATOR . $imageName,
                self::FORM_USERIMAGE_SIZE,
                'auto',
                !($useCrop),
                $thumbnailsDirectory,
                $useCrop
            );

            if (!is_dir($cropDirectory)) {
                Tools_Filesystem_Tools::mkdir($cropDirectory);
            }

            Tools_Image_Tools::resizeByParameters(
                $pathToDirectory . Tools_Image_Tools::FOLDER_ORIGINAL . DIRECTORY_SEPARATOR . $imageName,
                self::COMMENT_USERIMAGE_SIZE,
                'auto',
                !($useCrop),
                $cropDirectory,
                $useCrop
            );

            Comments_Mappers_User::getInstance()->save($uploadModel);

            echo json_encode(array('error' => false, 'responseText' => $imageName));

            return true;
        }
    }

    /**
     * @return bool
     *
     */
    public function deleteImageDataAction()
    {
        if ($this->_sessionHelper->getCurrentUser()->getRoleId() != Tools_Security_Acl::ROLE_GUEST) {
            $data       = $this->_request->getParams();
            $folderPath = realpath(
                $this->_websiteHelper->getPath() . $this->_websiteHelper->getMedia() . self::FOLDER_NAME
            );


            $mapper     = Comments_Mappers_User::getInstance();
            $userMapper = $mapper->find(
                $this->_sessionHelper->getCurrentUser()->getId()
            );
            $imageName  = $userMapper->getImageName();

            if ($mapper->find($this->_sessionHelper->getCurrentUser()->getId())) {
                $mapper->delete($this->_sessionHelper->getCurrentUser()->getId());
            }

            try {
                // removing image from filesystem
                if (($result = Tools_Image_Tools::removeImageFromFilesystem(
                        $imageName,
                        self::FOLDER_NAME
                    )) !== true
                ) {
                    $this->_error($result);
                }

                //cleaning up the file system if needed
                $folderContent = Tools_Filesystem_Tools::scanDirectory($folderPath, false, true);
                if (empty($folderContent)) {
                    try {
                        Tools_Filesystem_Tools::deleteDir($folderPath);
                    } catch (Exception $e) {
                        $this->_debugMode && error_log($e->getMessage());
                        $this->_error($e->getMessage());
                    }
                }
            } catch (Exceptions_SeotoasterException $e) {
                error_log($e->getMessage() . PHP_EOL . $e->getTraceAsString());
                $this->_error($e->getMessage());
            }
            echo json_encode(array('error' => false));

            return true;
        }
    }

    /**
     * ====
     * Admin methods.
     * ====
     */

    public function loadConfigsNameListAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $data = array();
            $list = Comments_Mappers_Configuration::getInstance()->fetchAll();
            foreach ($list as $val) {
                $data[] = $val;
            }
            echo json_encode($data);
        }
    }

    public function loadConfigsAction()
    {
        if ($this->_request->getPost() && Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $result = Comments_Mappers_Configuration::getInstance()->findByName(
                $this->_request->getParam('listName')
            );
            echo json_encode($result);
        }
    }

    public function saveConfigsAction()
    {
        if ($this->_request->getPost() && Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $configModel = new Comments_Models_Configuration($this->_request->getPost());
            $result      = Comments_Mappers_Configuration::getInstance()->save($configModel);

            if ($result) {
                echo json_encode(
                    array(
                        'responseText' => $this->_translator->translate('Configuration updated'),
                        'class'        => 'success',
                        'error'        => false
                    )
                );
            } else {
                echo json_encode(
                    array(
                        'responseText' => $this->_translator->translate('Cannot update configuration.<br> Maybe you didn\'t make a changes'),
                        'class'        => 'error',
                        'error'        => true
                    )
                );
            }
        }
    }

    public function configurationAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $this->_view->websiteUrl = $this->_websiteHelper->getUrl();
            echo $this->_view->render('panel.comments.configuration.phtml');
        }
    }

    public function configManagerAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $this->_view->websiteUrl = $this->_websiteHelper->getUrl();
            echo $this->_view->render('panel.comments.configManager.phtml');
        }
    }

    public static function setResponseMessage($caption, $text, $type = self::RESPONSE_TYPE_OK, $data = null, $code = Api_Service_Abstract::REST_STATUS_OK) {
        return array(
            'caption' => Zend_Registry::get('Zend_Translate')->translate($caption),
            'message' => Zend_Registry::get('Zend_Translate')->translate($text),
            'type' => $type,
            'data' => $data,
            'code' => $code
        );
    }
}
