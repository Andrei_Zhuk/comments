var commentsSystem = angular
    .module('CommentsSystem', ['ui.bootstrap', 'pascalprecht.translate', 'ngResource'], function ($httpProvider) {
        // Используем x-www-form-urlencoded Content-Type
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        // Переопределяем дефолтный transformRequest в $http-сервисе
        $httpProvider.defaults.transformRequest = [function (data) {
            /**
             * рабочая лошадка; преобразует объект в x-www-form-urlencoded строку.
             * @param {Object} obj
             * @return {String}
             */
            var param = function (obj) {
                var query = '';
                var name, value, fullSubName, subValue, innerObj, i;

                for (name in obj) {
                    value = obj[name];

                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value instanceof Object) {
                        for (subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
    })
    .config(function ($translateProvider) {
        $translateProvider.translations('en', {
            // button
            SEND: 'Send',
            CANCEL: 'Cancel',
            SAVE: 'Save',
            REPLY: 'Reply',
            PUBLISH: 'Publish',
            EDIT: 'Edit',
            REMOVE: 'Remove',
            REPLIES: 'Replies: {{count}}',
            SPAM: 'Spam',

            YOU_EMAIL: 'Your E-mail',
            YOU_FULLNAME: 'Your Full name',
            YOU_REPLY: 'Your reply',

            USER_STATUS: 'registered',

            // Pagination
            FIRST_PAGE: 'First',
            PREVIOUS_PAGE: 'Previous',
            NEXT_PAGE: 'Next',
            LAST_PAGE: 'Last'
        });
        $translateProvider.translations('ru', {
            // button
            SEND: 'Отправить',
            CANCEL: 'Отменить',
            SAVE: 'Сохранить',
            REPLY: 'Ответить',
            PUBLISH: 'Опубликовать',
            EDIT: 'Редактировать',
            REMOVE: 'Удалить',
            REPLIES: 'Ответов: {{count}}',
            SPAM: 'Спам',

            YOU_EMAIL: 'Ваш E-mail',
            YOU_FULLNAME: 'Ваше полное имя',
            YOU_REPLY: 'Ваш ответ',

            USER_STATUS: 'зарегистрирован',

            // Pagination
            FIRST_PAGE: 'Первая',
            PREVIOUS_PAGE: 'Предыдущая',
            NEXT_PAGE: 'Следующая',
            LAST_PAGE: 'Последняя'
        });
        var lang = window.document.documentElement.lang;
        $translateProvider.preferredLanguage(lang);
    })
    .run(function ($translate, paginationConfig) {
        $translate(['FIRST_PAGE', 'PREVIOUS_PAGE', 'NEXT_PAGE', 'LAST_PAGE']).then(function (translations) {
            paginationConfig.firstText = translations.FIRST_PAGE;
            paginationConfig.previousText = translations.PREVIOUS_PAGE;
            paginationConfig.nextText = translations.NEXT_PAGE;
            paginationConfig.lastText = translations.LAST_PAGE;
        });
    });

/*angular.element(document).ready(function () {
    angular.bootstrap(document, ['CommentsSystem']);
});*/

angular.element(document).ready(function() {
    angular.bootstrap(document.getElementById('appNgCommentsSystem'), ['CommentsSystem']);
});