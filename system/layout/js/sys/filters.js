angular.module('CommentsSystem')

    .filter('avatars', function () {
        return function (items) {
            var filtered = [];
            var url = document.getElementById('website_url').value;
            var imgDefault = url + 'plugins/comments/system/layout/img/default-small.jpg';
            var imgUser = url + 'media/commentsSystem-userImages/crop/';

            angular.forEach(items, function (item) {
                if (item.imageName === null) {
                    item.imageUrl = imgDefault;
                } else {
                    item.imageUrl = imgUser + item.imageName;
                }
                filtered.push(item);
                //console.log(item);
            });
            return filtered;
        }
    })
    .filter('list', function () {
        return function (items, admin, uid) {
            var filtered = [];
            var num = 0;
            angular.forEach(items, function (item) {
                    if (item.published == 1 || admin || item.userId == uid) {
                        if (item.replyId == 0) {
                            item.subComments = [];

                            angular.forEach(items, function (sub) {
                                if (sub.replyId == item.id) {
                                    if (sub.published == 1 || admin || sub.userId == uid) {
                                        item.subComments.push(sub);
                                    }
                                }
                            });
                            // TODO need fix, because very bad huck
                            if (typeof item.datePublished == 'string') {
                                item.datePublished = Date.parse(item.datePublished.replace(" ", "T"));
                            }
                            item.number = ++num;
                            filtered.push(item);
                        }
                    }
                }
            )
            ;
            return filtered;
        }
    })
    .filter('slice', function () {
        return function (data, currentPage, limit) {
            if (data) {
                var start = ((currentPage - 1) * limit),
                    end = start + limit;
                return data.slice(start, end);
            }
            return [];
        };
    });