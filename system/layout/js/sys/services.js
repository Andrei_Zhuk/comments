angular.module('CommentsConfig')

    .factory('commentsConfigFactory', function ($http) {
        return {
            loadConfigsNameList: function () {
                var promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/loadConfigsNameList")
                    .then(function (response) {
                        return response.data;
                    });
                return promise;
            },
            saveConfigs: function (data) {
                var promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/saveConfigs", data)
                    .then(function (response) {
                        return response.data;
                    });
                return promise;
            },
            loadConfigs: function (item) {
                var promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/loadConfigs", {listName: item.listName})
                    .then(function (response) {
                        return response.data;
                    });
                return promise;
            }
        }
    });