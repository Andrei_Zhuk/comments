angular.module('CommentsSystem')

    .directive('commentslist', function () {
        return {
            templateUrl: document.getElementById('website_url').value + 'plugin/comments/run/getCommentsListTemplate/',
            replace    : true
        }
    })
    .directive('replyComment', function () {
        return {
            templateUrl: document.getElementById('website_url').value + 'plugin/comments/run/getReplyTemplate/',
            replace    : true
        }
    })

    .directive('editComment', function ($translate) {
        return {
            restrict: 'E',
            template: '' +
            '<form class="comment-edit" ng-show="comment.editing" ng-submit="saveEdit(comment)">' +
                '<p><textarea ng-model="comment.message">{{comment.message}}</textarea></p>' +
                '<p>' +
                    '<a translate="CANCEL" class="btn" ng-click="cancelEdit(comment)"></a>' +
                    '<button translate="SAVE" type="submit"></button>' +
                '</p>' +
            '</form>',
            replace : true
        }
    })
    .directive('editSubComment', function () {
        return {
            restrict: 'E',
            template: '' +
            '<form class="comment-edit" ng-show="subComment.editing" ng-submit="saveEdit(subComment)">' +
            '<p><textarea ng-model="subComment.message">{{subComment.message}}</textarea></p>' +
            '<p>' +
            '<a translate="CANCEL" class="btn" ng-click="cancelEdit(subComment)"></a>' +
            '<button translate="SAVE" type="submit"></button>' +
            '</p>' +
            '</form>',
            replace : true
        }
    });;