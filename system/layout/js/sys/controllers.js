angular.module('CommentsConfig')

    .controller('CommentsConfigCtrl', function ($scope, commentsConfigFactory) {
        $scope.commentsCurrentItem = null;
        $scope.form = {};


        $scope.loadConfigsList = function () {
            commentsConfigFactory.loadConfigsNameList()
                .then(function (response) {
                    $scope.configsList = response;
                });
        }

        $scope.changeCommentsName = function () {
            showSpinner();
            if ($scope.commentsCurrentItem) {
                commentsConfigFactory.loadConfigs($scope.commentsCurrentItem)
                    .then(function (response) {
                        $scope.form = response;
                        hideSpinner();
                    });
            } else {
                $scope.form = {};
                hideSpinner();
            }
        }

        $scope.saveConfigs = function () {
            showSpinner();
            commentsConfigFactory.saveConfigs($scope.form)
                .then(function (response) {
                    hideSpinner();
                    showMessage(response.responseText, response.error);
                });
        }

    });