angular.module('CommentsSystem')

    .factory('commentsListApiService', function ($resource) {
        return $resource(document.getElementById('website_url').value + 'api/comments/list', null, {
            get: {
                method: 'GET',
                params: {},
                isArray: false
            },
            post: {
                method: 'POST'
            }
        });
        /*return $resource(document.getElementById('website_url').value + 'api/comments/list/id/:id', null, {
         getList: {
         method: 'GET',
         params: {id: '@id'},
         isArray: false
         },
         getItem: {
         method: 'GET',
         //params: {apiName: 'get', ln: '@ln', lt: '@lt', pid: document.getElementById('page_id').value},
         isArray: false
         }
         });*/
    })


    /*.factory('commentsApiService', function ($resource) {
     return $resource(document.getElementById('website_url').value + 'api/comments/rating/:apiName/:id', {
     apiName: '@apiName',
     id: '@id'
     }, {
     get: {
     method: 'GET',
     params: {apiName: 'get', userId: '@userId', pageId: document.getElementById('page_id').value},
     isArray: false
     }
     })

     })*/


    .factory('commentsActionService', function ($rootScope, $http, $q, $filter) {

        function _getData(pageId, name, type) {
            var deferred = $q.defer();
            $http.post(document.getElementById('website_url').value + "plugin/comments/run/getCommentsList", {
                pageId: pageId,
                listName: name,
                listType: type
            }).then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        /**
         *
         * @param data
         * @returns {*}
         * @private
         */
        function _filterByList(data) {
            var result = $filter('list')(data, {admin: $rootScope.isAdmin, uid: $rootScope.uid});
            return result;
        }

        /**
         *
         * @param data
         * @private
         */
        function _checkLength(data) {
            $rootScope.totalItems = data.length;
        }

        /**
         *
         * @param data
         * @returns {*}
         * @private
         */
        function _listSlice(data) {
            return $filter('slice')(data, $rootScope.currentPage, $rootScope.itemsPerPage);
        }

        /**
         *
         * @param data
         * @returns {*}
         * @private
         */
        function _preFormatting(data) {
            var list;

            list = $filter('orderBy')(data, 'datePublished', true);
            // save filtered by date(making the the first comment on the last page when pagination is on) list
            // we will work with this list after
            $rootScope.preliminaryList = list;

            return list;
        }

        function _formattingList(data) {
            var filteredList, slicedList, result;

            filteredList = _filterByList(data);
            _checkLength(filteredList);
            slicedList = _listSlice(filteredList);
            result = slicedList;

            return result;
        }

        function _spliceItem(item) {
            var index;
            // Find the element
            index = $rootScope.preliminaryList.indexOf(item);
            // Remove element form array
            $rootScope.preliminaryList.splice(index, 1);
            return $rootScope.preliminaryList;
        }

        return {
            renderList: function (pageId, name, type) {
                var promise, result, preFormatting;

                promise = _getData(pageId, name, type).then(function (unFormattedData) {
                    // get unFormatted data and work with it
                    preFormatting = _preFormatting(unFormattedData.data);
                    // formatting data for nice list and send to view
                    result = _formattingList(preFormatting);
                    return result;
                });
                return promise;
            },
            addItem: function (data) {
                var promise, result;

                promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/addComment", data)
                    .then(function (response) {
                        result = response.data;
                        return result;
                    });
                return promise;
            },
            removeItem: function (item) {
                var promise, result, splicedList;

                promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/removeComment", {id: item.id})
                    .then(function (response) {
                        splicedList = _spliceItem(item);
                        result = _formattingList(splicedList);
                        return result;
                    });
                return promise;
            },
            editItem: function (id, message) {
                var promise;

                promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/editComment", {
                    id: id,
                    message: message
                })
                    .then(function (response) {
                        return response.data;
                    });
                return promise;
            },
            publishItem: function (id) {
                var promise;

                promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/publishComment", {id: id})
                    .then(function (response) {
                        return response.data;
                    });
                return promise;
            },
            moveToSpam: function (item) {
                var promise, result, splicedList;

                promise = $http.post(document.getElementById('website_url').value + "plugin/comments/run/moveToSpam", {id: item.id})
                    .then(function (response) {
                        splicedList = _spliceItem(item);
                        result = _formattingList(splicedList);
                        return result;
                    });
                return promise;
            }
        }
    })

    .service('commentsService', function ($rootScope, $http) {
        return {
            setRating: function (data) {
                var request = $http.post(document.getElementById('website_url').value + "plugin/comments/run/changeRating", data)
                    .then(function (response) {
                        return response.data;
                    });
                return request
            },
            rating: function (val, event) {
                if (event === 'inc') {
                    return ++val;
                } else if (event === 'dec') {
                    return --val;
                }
            }
        }
    });