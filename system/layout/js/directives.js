angular.module('CommentsSystem')

    .directive('editCommentDirective', function () {
        return {
            restrict: 'A',
            template: '' +
            '<form class="comment-edit" data-ng-show="item.editing" data-ng-submit="saveEdit(item)">' +
                '<p><textarea data-ng-model="item.message">{{item.message}}</textarea></p>' +
                '<p>' +
                    '<a data-translate="CANCEL" class="btn" data-ng-click="cancelEdit(item)"></a>' +
                    '<button data-translate="SAVE" type="submit"></button>' +
                '</p>' +
            '</form>',
            replace: true,
            link: function(scope) {
                scope.item = scope.subComment || scope.comment;
            }
        }
    })

    .directive('replyCommentDirective', function(){
        return {
            restrict: 'A',
            //templateUrl: document.getElementById('website_url').value + 'plugin/comments/run/getReplyTemplate/',
            template: '' +
            '<div class="send-reply-box">' +
                '<p class="messages-list" data-ng-repeat="message in comment.formMessages" data-ng-show="comment.formMessages"> ' +
                    '<span data-ng-bind="message.responseText" class="{{message.class}} message"></span>' +
                '</p>' +
                '<form data-ng-submit="sendReply(comment)" data-ng-show="comment.replied"> ' +
                    '<p data-ng-if="!isUser"> ' +
                        '<label class="required" data-translate="YOU_FULLNAME"></label> ' +
                        '<input type="text" name="fullName" data-ng-model="comment.rForm.fullName" /> ' +
                    '</p> ' +
                    '<p data-ng-if="!isUser"> ' +
                        '<label class="required" data-translate="YOU_EMAIL"></label> ' +
                        '<input type="text" name="fullName" data-ng-model="comment.rForm.email" /> ' +
                    '</p> ' +
                    '<p> ' +
                        '<label class="required" data-translate="YOU_REPLY"></label> ' +
                        '<textarea name="message" data-ng-model="comment.rForm.message"></textarea></p> ' +
                    '<p> ' +
                        '<a class="btn" data-ng-click="cancelReply(comment)" data-translate="CANCEL"></a> ' +
                        '<button type="submit" data-translate="SEND"></button> ' +
                    '</p> ' +
                '</form>' +
            '</div>',
            replace: true
        }
    })

    .directive('commentslist', function () {
        return {
            templateUrl: document.getElementById('website_url').value + 'plugin/comments/run/getCommentsListTemplate/',
            replace: true
        }
    });