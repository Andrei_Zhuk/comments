angular.module('CommentsSystem')

    .controller('FormCtrl', function ($rootScope, $scope, $timeout, $filter, formCtrlParams, commentsActionService) {
        $scope.sForm = formCtrlParams;
        $scope.sForm.pageId = document.getElementById('page_id').value;
        $scope.isUser = formCtrlParams.isUser;
        $scope.isAdmin = formCtrlParams.isAdmin;
        $scope.uid = formCtrlParams.uid;
        $scope.sendDisabled = false;

        /**
         * Send message
         *
         */
        $scope.sendComment = function () {
            // disable button
            $scope.sendDisabled = true;

            if(formCtrlParams.captcha) {
                var array = $('#appNgCommentsSystem form').serializeArray();

                for(var key in array) {
                    if(array[key].name == 'g-recaptcha-response'){
                        var captcha_key = array[key].value;
                    }
                }
                $scope.sForm.captchaKey = captcha_key;
            }

            commentsActionService.addItem($scope.sForm).then(function (response) {
                // show message or messages
                $scope.formMessages = response;

                commentsActionService.renderList($scope.sForm.pageId, $scope.sForm.listName, $scope.sForm.listType).then(function (response) {
                    $rootScope.commentsList = response;
                });

                // clear fields and enable button
                // $scope.sForm.message = $scope.sForm.positive = $scope.sForm.negative = null;
                if($scope.formMessages.length == 1 && formCtrlParams.captcha){
                    if($scope.formMessages[0].class == 'success'){
                        $scope.sForm.message = $scope.sForm.positive = $scope.sForm.negative = null;
                        grecaptcha.reset();
                    }
                }
                $scope.sendDisabled = false;

                // hide messages after 4 sec.
                $timeout(function () {
                    $scope.formMessages = null;
                }, 4000);
            });
        };
    })

    .controller('ListCtrl', function ($rootScope, $scope, $timeout, $filter, $translate, $resource, listCtrlParams, commentsService, commentsActionService, commentsListApiService) {
        // Variables:
        $scope.timeForMessages = 1500;
        $scope._timerReply = null;

        $scope.pageId = document.getElementById('page_id').value;
        $scope.cList = listCtrlParams;
        $scope.cList.pageId = $scope.pageId;

        $rootScope.uid = listCtrlParams.uid;
        $rootScope.isAdmin = listCtrlParams.isAdmin;
        $scope.isUser = listCtrlParams.isUser;
        $scope.rating = listCtrlParams.rating;
        $scope.initPag = null;
        $scope.dateFormat = listCtrlParams.dateFormat;
        $scope.avatars = listCtrlParams.avatars;

        // Variables.Sorting
        $scope.predicate = 'datePublished';
        $scope.orderInvert = listCtrlParams.orderInvert == 1 ? true : false;

        // Variables.Pagination:
        $rootScope.totalItems = 0;
        $rootScope.itemsPerPage = listCtrlParams.limit;
        $rootScope.currentPage = 1;

        /**
         * Init load list
         */
        $scope.initLoadList = function () {
            commentsActionService.renderList($scope.cList.pageId, $scope.cList.listName, $scope.cList.listType).then(function (response) {
                // push filtered and structured list to template
                $rootScope.commentsList = response;
            });
        };

        var data = {};
        data.ln = $scope.cList.listName;
        data.lt = $scope.cList.listType;
        data.pid = $scope.cList.pageId;

        commentsListApiService.get(data);
        commentsListApiService.get({cid: 62});
        commentsListApiService.post(data);

        /**
         * Pagination
         */
        if (!angular.isDefined($rootScope.preliminaryList)) {
            $scope.$watch('currentPage + itemsPerPage', function () {
                $rootScope.commentsList = $filter('slice')($rootScope.preliminaryList, $scope.currentPage, $rootScope.itemsPerPage);
            });
            $scope.initPag = true;
        }
        /*$scope.pageCount = function () {
         return Math.ceil($rootScope.preliminaryList.length / $rootScope.itemsPerPage);
         };*/

        /**
         * Remove comment and subComment
         */
        $scope.remove = function (item) {
            commentsActionService.removeItem(item).then(function (response) {
                $rootScope.commentsList = response;
            });
        };


        /**
         * Edit comment
         *
         */
        $scope.editing = false;

        $scope.edit = function (item) {
            $scope.originalMessage = item.message;
            item.editing = true;
        };

        $scope.saveEdit = function (item) {
            commentsActionService.editItem(item.id, item.message)
                .then(function (response) {
                    if (response.error) {
                        item.message = $scope.originalMessage;
                    }
                    item.editing = false;
                });
        };

        $scope.cancelEdit = function (item) {
            item.message = $scope.originalMessage;
            item.editing = false;
        };

        /**
         * Reply comment
         *
         */
        $scope.replied = false;

        $scope.reply = function (item) {
            $timeout.cancel($scope._timerReply);

            item.formMessages = null;
            item.replied = true;
        };

        $scope.sendReply = function (item) {
            var rFormData = item.rForm ? item.rForm : {};
            rFormData.pageId = $scope.pageId;
            rFormData.replyId = item.id;
            rFormData.listName = item.listName;
            rFormData.listType = item.listType;

            // TODO reorganize
            commentsActionService.addItem(rFormData).then(function (response) {
                // show message or messages
                item.formMessages = response;

                // if hasn't errors
                if (!response[0].error) {
                    item.replied = false;
                    $scope._timerReply = $timeout(function () {
                        commentsActionService.renderList($scope.cList.pageId, $scope.cList.listName, $scope.cList.listType)
                            .then(function (response) {
                                $rootScope.commentsList = response;
                                item.formMessages = null;
                            });

                    }, $scope.timeForMessages);
                } else {
                    $scope._timerReply = $timeout(function () {
                        item.formMessages = null;
                    }, $scope.timeForMessages);
                }

            });
        };

        $scope.cancelReply = function (item) {
            item.replied = false;
            item.formMessages = null;
        };

        /**
         * Publish comment
         *
         */
        $scope.publish = function (item) {
            commentsActionService.publishItem(item.id).then(function (response) {
                if (!response.error) {
                    item.published = 1;
                }
            });
        };

        /**
         * Move to spam
         * @param item
         */
        $scope.spam = function (item) {
            commentsActionService.moveToSpam(item)
                .then(function (response) {
                    $rootScope.commentsList = response;
                });
        };

        /**
         * Rating method
         *
         */
        $scope.changeRat = function (item, event) {
            var data = {};
            data.id = item.id;
            data.value = commentsService.rating(item.rating, event);
            data.oldRating = item.rating;
            item.rating = data.value;

            commentsService.setRating(data).then(function (response) {
                if (response.error) {
                    showMessage(response.responseText);
                    item.rating = data.oldRating;
                }
            });

        };

        /**
         *
         * @param item
         * @returns {boolean|*}
         */
        $scope.visible = function (item) {
            var result = item.published == 1 || $scope.isAdmin || item.userId == $scope.uid;
            return result;
        };
    });