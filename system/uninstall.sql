DROP TABLE IF EXISTS `plugin_comments_list`;
DROP TABLE IF EXISTS `plugin_comments_user`;
DROP TABLE IF EXISTS `plugin_comments_rating`;
DROP TABLE IF EXISTS `plugin_comments_configuration`;

DELETE FROM `email_triggers_actions` WHERE `trigger`='c_newcomment';
DELETE FROM `email_triggers` WHERE `trigger_name`='c_newcomment';

DELETE FROM `email_triggers_actions` WHERE `trigger`='c_newreply';
DELETE FROM `email_triggers` WHERE `trigger_name`='c_newreply';

