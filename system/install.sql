CREATE TABLE IF NOT EXISTS `plugin_comments_list` (
  `listName`      VARCHAR(255)
                  COLLATE utf8_unicode_ci               NOT NULL,
  `listType`      INT(10) UNSIGNED                      NOT NULL,
  `pageId`        INT(10) UNSIGNED                      NOT NULL DEFAULT '0',
  `id`            INT(10) UNSIGNED                      NOT NULL AUTO_INCREMENT,
  `replyId`       INT(10)                               NOT NULL DEFAULT '-1',
  `userId`        INT(10) UNSIGNED                      NOT NULL DEFAULT '0',
  `fullName`      VARCHAR(255)
                  COLLATE utf8_unicode_ci               NOT NULL,
  `email`         VARCHAR(255)
                  COLLATE utf8_unicode_ci               NOT NULL,
  `message`       TEXT
                  COLLATE utf8_unicode_ci               NOT NULL,
  `positive`      VARCHAR(255)
                  COLLATE utf8_unicode_ci,
  `negative`      VARCHAR(255)
                  COLLATE utf8_unicode_ci,
  `datePublished` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating`        INT(10)                               NOT NULL DEFAULT '0',
  `ipAddress`     VARCHAR(255)
                  COLLATE utf8_unicode_ci               NOT NULL,
  `published`     INT(10) UNSIGNED                      NOT NULL DEFAULT '0',
  `spam`          INT(10) UNSIGNED                      NOT NULL DEFAULT '0',

  PRIMARY KEY (`id`),
  KEY `listName` (`listName`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `plugin_comments_spam` (
  `id`      INT(10) UNSIGNED        NOT NULL AUTO_INCREMENT,
  `userId`  INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `message` TEXT
            COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `plugin_comments_user` (
  `id`        INT(10) UNSIGNED        NOT NULL AUTO_INCREMENT,
  `imageName` VARCHAR(255)
              COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `plugin_comments_rating` (
  `id`        INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId`    INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `value`     INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `commentId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `plugin_comments_configuration` (
  `id`            INT(10) UNSIGNED        NOT NULL AUTO_INCREMENT,
  `listName`      VARCHAR(255)
                  COLLATE utf8_unicode_ci NOT NULL,
  `listType`      INT(10) UNSIGNED        NULL,
  `pageId`        INT(10) UNSIGNED        NULL,
  `uploader`      INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `aspects`       INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `noModerate`    INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `btnOnlyLogged` INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `onlyLogged`    INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `rating`        INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `avatars`       INT(10) UNSIGNED        NOT NULL DEFAULT '0',
  `limit`         INT(10) UNSIGNED        NOT NULL DEFAULT '15',
  `dateFormat`    VARCHAR(255)
                  COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MMMM dd, yyyy',
  `orderInvert`   INT(10) UNSIGNED        NOT NULL DEFAULT '0',

  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;


INSERT INTO `email_triggers` (`enabled`, `trigger_name`, `observer`) VALUES
  ('1', 'c_newcomment', 'Comments_MailWatchdog'),
  ('1', 'c_newreply', 'Comments_MailWatchdog');