<?php

class Widgets_Comments_Comments extends Widgets_Abstract
{
    protected $_cacheable = false;

    protected $_listName;
    protected $_listType = self::TYPE_REGULAR;

    /**
     * Widget params
     * @var
     */
    protected $_rating;
    protected $_aspects;
    protected $_avatars;
    protected $_uploader;
    protected $_noModerate;
    protected $_btnOnlyLogged;
    protected $_onlyLogged;
    protected $_limit      = self::DEFAULT_LIMIT_NUMBER;
    protected $_orderInvert;
    protected $_dateFormat = self::DATE_FORMAT_EN;

    /**
     * Uploader default settings
     */
    const FOLDER_NAME = 'commentsSystem-userImages';
    const FORM_IMAGE_SIZE = 150;

    /**
     * Default date format
     */
    const DATE_FORMAT_EN = 'MMMM dd, yyyy';
    const DATE_FORMAT_RU = 'dd MMMM, yyyy';

    /**
     * Type of containers
     */
    const TYPE_REGULAR = 1;
    const TYPE_STATIC = 2;

    /**
     * Default number of comments limit on the page
     */
    const DEFAULT_LIMIT_NUMBER = 15;

    const COMMENTS_TYPE_FORM = 'form';
    const COMMENTS_TYPE_LIST = 'list';

    protected $_isAdmin;
    protected $_isUser;
    protected $_isGuest;
    protected $_configs;
    protected $_websiteHelper;
    protected $_sessionHelper;
    protected $_currentUser;
    protected $_configsParams;
    protected $_captcha;

    /**
     * Init method.
     *
     */
    protected function _init()
    {
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getExistingHelper('website');
        $this->_sessionHelper = Zend_Controller_Action_HelperBroker::getExistingHelper('session');

        $this->_view = new Zend_View(array('scriptPath' => __DIR__ . '/views'));
        $this->_view->setHelperPath(APPLICATION_PATH . '/views/helpers/');
        $this->_view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $this->_view->addScriptPath($this->_websiteHelper->getPath() . 'seotoaster_core/application/views/scripts/');

        $this->_currentUser = $this->_sessionHelper->getCurrentUser();
        $this->_isAdmin     = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT);
        $this->_isUser      = $this->_currentUser->getRoleId() != Tools_Security_Acl::ROLE_GUEST ? true : false;
        $this->_isGuest     = $this->_currentUser->getRoleId() == Tools_Security_Acl::ROLE_GUEST ? true : false;
        $this->_configs     = Application_Model_Mappers_ConfigMapper::getInstance()->getConfig();
        if ($this->_isUser) {
            if ($this->_currentUser->getFullName()) {
                $this->_userName = $this->_currentUser->getFullName();
            } else {
                $this->_userName = $this->_currentUser->getRoleId();
            }
        }
    }

    /**
     * Load method.
     *
     * @return string
     * @throws Exceptions_SeotoasterWidgetException
     */
    protected function _load()
    {
        if (empty($this->_options)) {
            throw new Exceptions_SeotoasterWidgetException(
                $this->_translator->translate(
                    'Not enough parameters for the widget.'
                )
            );
        }

        $methodName = '_render' . ucfirst(array_shift($this->_options));
        if (method_exists($this, $methodName)) {

            return $this->$methodName();
        } else {
            return 'Sorry.';
        }
    }

    /**
     * Check option method.
     *
     * @param      $optionName
     * @param null $callback
     * @return bool|null
     */
    protected function _checkOption($optionName, $callback = null)
    {
        if (in_array($optionName, $this->_options)) {
            unset($this->_options[array_search($optionName, $this->_options)]);
            if ($callback == null) {
                return true;
            } else {
                return $callback;
            }
        } else {
            return $this->_configsParams[$optionName];
        }
    }

    protected function _findOption($optionName, $callback = null)
    {
        // return true / false
        if (in_array($optionName, $this->_options)) {
            unset($this->_options[array_search($optionName, $this->_options)]);
            if ($callback === null) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return $this->_configsParams[$optionName];
        }
    }

    /**
     * @return mixed
     */
    protected function _getConfigs()
    {
        $configModel   = new Comments_Models_Configuration();
        $configMapper  = Comments_Mappers_Configuration::getInstance();
        $configsParams = $configMapper->findByName($this->_listName);
        if (!$configsParams) {
            $configModel->setListName($this->_listName);
            $configsParams = $configMapper->save($configModel);
        }
        return $configsParams;
    }


    protected function _renderNgApp()
    {
        return ' id="appNgCommentsSystem" ';
    }

    /**
     * Render form method.
     * {$comments:form: name[string] : avatars[true|false] : aspects[true|false] : noModerate[true|false] : onlyLogged[true|false] : static }
     * @throws Exceptions_SeotoasterWidgetException
     */
    protected function _renderForm()
    {
        if (empty($this->_options)) {
            throw new Exceptions_SeotoasterWidgetException(
                $this->_translator->translate('Not enough parameters for the widget.')
            );
        }
        // Get params
        $this->_listName = array_shift($this->_options);
        if (in_array('static', $this->_options)) {
            $this->_listType = self::TYPE_STATIC;
            unset($this->_options[array_search('static', $this->_options)]);
        }

        $this->_configsParams = $this->_getConfigs();

        $this->_aspects       = $this->_findOption('aspects');
        $this->_uploader      = $this->_findOption('uploader');
        $this->_noModerate    = $this->_findOption('noModerate');
        $this->_btnOnlyLogged = $this->_findOption('btnOnlyLogged');
        $this->_captcha       = $this->_findOption('captcha');

        // AngularJs params
        $widgetCtrlParams = array(
            'listName'   => $this->_listName,
            'listType'   => $this->_listType,
            'fullName'   => $this->_isUser ? $this->_userName : null,
            'email'      => $this->_isUser ? $this->_currentUser->getEmail() : null,
            'userId'     => $this->_isUser ? $this->_currentUser->getId() : null,
            'replyId'    => 0,
            'isUser'     => $this->_isUser,
            'isAdmin'    => $this->_isAdmin,
            'noModerate' => $this->_noModerate,
            'uid'        => $this->_isUser ? $this->_currentUser->getId() : null,
            'captcha'    => $this->_captcha ? true : false,
        );

        // Views
        $this->_view->formId           = $this->_listName;
        $this->_view->aspects          = $this->_aspects;
        $this->_view->isUser           = $this->_isUser;
        $this->_view->btnOnlyLogged    = $this->_btnOnlyLogged;
        $this->_view->websiteUrl       = $this->_websiteHelper->getUrl();
        $this->_view->uploader         = ($this->_uploader && $this->_isUser) ? $this->_renderUploader() : null;
        $this->_view->lang             = $this->_configs["language"];
        $this->_view->widgetCtrlParams = json_encode($widgetCtrlParams);
        $this->_view->captcha       = $this->_captcha;

        // Render
        /*if ($this->_onlyLogged && $this->_isGuest) {
            $view = $this->_view->render('widget.comments.form.without.phtml');
        } else {
            $view = $this->_view->render('widget.comments.form.phtml');
        }*/
        $view = $this->_view->render('widget.comments.form.phtml');

        return $view;
    }

    /**
     * Render list method.
     * {$comments:list: name[string] : rating[true|false] : avatars[true|false] : limit=? : orderInvert[true|false] : static }
     */
    protected function _renderList()
    {
        if (empty($this->_options)) {
            throw new Exceptions_SeotoasterWidgetException(
                $this->_translator->translate('Not enough parameters for the widget.')
            );
        }

        $this->_listName = array_shift($this->_options);
        if (in_array('static', $this->_options)) {
            $this->_listType = self::TYPE_STATIC;
            unset($this->_options[array_search('static', $this->_options)]);
        }

        $this->_configsParams = $this->_getConfigs();

        $this->_rating     = $this->_checkOption('rating');
        $this->_avatars    = $this->_checkOption('avatars');
        $this->_onlyLogged = $this->_checkOption('onlyLogged');
        $this->_dateFormat = $this->_configsParams['dateFormat'];
        if (in_array('orderInvert', $this->_options)) {
            $this->_orderInvert = 1;
            unset($this->_options[array_search('orderInvert', $this->_options)]);
        } else {
            $this->_orderInvert = $this->_configsParams['orderInvert'];
        }

        $limit = current(preg_grep('/limit=*/', $this->_options));
        if ($limit) {
            $this->_limit = preg_replace('/limit=/', '', $limit);
        } else {
            $this->_limit = $this->_configsParams['limit'];
        }

        // AngularJs params
        $widgetCtrlParams = array(
            'listName'    => $this->_listName,
            'listType'    => $this->_listType,
            'published'   => 1,
            'isAdmin'     => $this->_isAdmin,
            'isUser'      => $this->_isUser,
            'uid'         => $this->_isUser ? $this->_currentUser->getId() : null,
            'rating'      => $this->_rating,
            'avatars'     => $this->_avatars,
            'limit'       => (int)$this->_limit,
            'orderInvert' => $this->_orderInvert,
            'dateFormat'  => $this->_dateFormat,
        );

        // Views:
        $this->_view->listId           = $this->_listName;
        $this->_view->websiteUrl       = $this->_websiteHelper->getUrl();
        $this->_view->lang             = $this->_configs["language"];
        $this->_view->widgetCtrlParams = json_encode($widgetCtrlParams);

        // Render
        if ($this->_onlyLogged && $this->_isGuest) {
            $view = $this->_view->render('widget.comments.list.without.phtml');
        } else {
            $view = $this->_view->render('widget.comments.list.phtml');
        }

        return $view;
    }


    protected function _renderUploader()
    {
        $userMapper = Comments_Mappers_User::getInstance()->find($this->_currentUser->getId());
        if ($userMapper) {
            $this->_view->imageName = $userMapper->getImageName();
        }

        // Views
        $this->_view->websiteUrl        = $this->_websiteHelper->getUrl();
        $this->_view->folderName        = self::FOLDER_NAME;
        $this->_view->defaultImageWidth = self::FORM_IMAGE_SIZE;
        $this->_view->userId            = $this->_currentUser->getId();
        $this->_view->userFullName      = $this->_userName;

        return $this->_view->render('widget.comments.uploader.phtml');
    }


    protected function _renderCounter(){
        if (empty($this->_options)) {
            throw new Exceptions_SeotoasterWidgetException(
                $this->_translator->translate('Not enough parameters for the widget.')
            );
        }

        $this->_listName = array_shift($this->_options);
        if (in_array('static', $this->_options)) {
            $this->_listType = self::TYPE_STATIC;
            unset($this->_options[array_search('static', $this->_options)]);
        }

        $this->_configsParams = $this->_getConfigs();


        $commentMapper = Comments_Mappers_Comment::getInstance();
        $list  = $commentMapper->fetchAll(array('listName = ?' => $this->_listName,'published = ?' => 1));


        $this->_view->number          = count($list);

        // Render
        $view = $this->_view->render('widget.comments.counter.phtml');

        return $view;
    }


    /*public static function getWidgetMakerContent()
    {
        $translator    = Zend_Registry::get('Zend_Translate');
        $view          = new Zend_View(array('scriptPath' => dirname(__FILE__) . '/views'));
        $websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $data          = array(
            'title'   => $translator->translate('Comments System'),
            'icons'   => array($websiteHelper->getUrl() . 'system/images/widgets/featured.png'),
            'content' => $view->render('wmcontent.phtml')
        );
        unset($view, $translator);

        return $data;
    }*/
}