<?php

class Api_Comments_Rating extends Api_Service_Abstract
{
    protected $_ratingMapper = null;

    /**
     * @var array
     */
    protected $_accessList = array(
        Tools_Security_Acl::ROLE_GUEST      => array(
            'allow' => array('get', 'post', 'put', 'delete')
        ),
        Tools_Security_Acl::ROLE_MEMBER     => array(
            'allow' => array('get', 'post', 'put', 'delete')
        ),
        Tools_Security_Acl::ROLE_ADMIN      => array(
            'allow' => array('get', 'post', 'put', 'delete')
        ),
        Tools_Security_Acl::ROLE_SUPERADMIN => array(
            'allow' => array('get', 'post', 'put', 'delete')
        )
    );

    /**
     *
     *
     */
    public function init()
    {
        $this->_ratingMapper = Comments_Mappers_Rating::getInstance();
    }

    /**
     *
     *
     */
    public function getAction()
    {
        $userId  = $this->_request->getParam('userId');
        $pageId  = $this->_request->getParam('pageId');
        $options = $ratingList = $this->_ratingMapper->fetchByUserIdOnPage($userId, $pageId);

        return Comments::setResponseMessage(
            Comments::SUCCESS_CAPTION,
            'Available options',
            Comments::RESPONSE_TYPE_OK,
            json_encode($options)
        );
    }

    /**
     *
     *
     */
    public function postAction()
    {

    }

    /**
     *
     *
     */
    public function putAction()
    {

    }

    /**
     *
     *
     */
    public function deleteAction()
    {

    }

}