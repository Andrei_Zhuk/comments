<?php

class Api_Comments_List extends Api_Service_Abstract
{
    protected $_ratingMapper   = null;
    protected $_commentsMapper = null;

    /**
     * @var array
     */
    protected $_accessList = array(
        Tools_Security_Acl::ROLE_GUEST      => array(
            'allow' => array('get')
        ),
        Tools_Security_Acl::ROLE_MEMBER     => array(
            'allow' => array('get')
        ),
        Tools_Security_Acl::ROLE_ADMIN      => array(
            'allow' => array('get', 'post', 'put', 'delete')
        ),
        Tools_Security_Acl::ROLE_SUPERADMIN => array(
            'allow' => array('get', 'post', 'put', 'delete')
        )
    );

    /**
     *
     *
     */
    public function init()
    {
        $this->_ratingMapper   = Comments_Mappers_Rating::getInstance();
        $this->_commentsMapper = Comments_Mappers_Comment::getInstance();

        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
    }

    /**
     *
     *
     */
    public function getAction()
    {
        // ../list? ln=name & lt=type & pid=id - all comments on the page
        // ../list? cid=id - one comment

        $params = $this->_request->getParams();


        /*$params = $this->_request->getParams();

        if (isset($params['id'])) {
            $id     = $params['id'];
            $result = $this->_commentsMapper->getComment($id);
        } else {

            $name = $params['ln'];
            $type = $params['lt'];
            $pid  = $params['pid'];

            $result = $this->_commentsMapper->getComments($name, $type, $pid);
        }
        return Comments::setResponseMessage(
            Comments::SUCCESS_CAPTION,
            'Available options',
            Comments::RESPONSE_TYPE_OK,
            $result
        );*/
    }

    /**
     *
     *
     */
    public function postAction()
    {

    }

    /**
     *
     *
     */
    public function putAction()
    {

    }

    /**
     *
     *
     */
    public function deleteAction()
    {

    }

}