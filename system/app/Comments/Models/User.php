<?php

class Comments_Models_User extends Application_Model_Models_Abstract
{
    protected $_id;
    protected $_imageName;

    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setImageName($imageName)
    {
        $this->_imageName = $imageName;

        return $this;
    }

    public function getImageName()
    {
        return $this->_imageName;
    }
}