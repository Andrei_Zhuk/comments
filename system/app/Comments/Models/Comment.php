<?php

class Comments_Models_Comment extends Application_Model_Models_Abstract
{
    protected $_listName;
    protected $_listType;
    protected $_pageId;
    protected $_id;
    protected $_replyId   = 0;
    protected $_userId    = -1;
    protected $_fullName;
    protected $_email;
    protected $_message;
    protected $_positive;
    protected $_negative;
    protected $_datePublished;
    protected $_rating    = 0;
    protected $_ipAddress;
    protected $_published = 0;
    protected $_spam      = 0;
    protected $_signUp    = 0;
    protected $_captcha;


    public function setListName($listName)
    {
        $this->_listName = $listName;

        return $this;
    }

    public function getListName()
    {
        return $this->_listName;
    }

    public function setListType($listType)
    {
        $this->_listType = $listType;

        return $this;
    }

    public function getListType()
    {
        return $this->_listType;
    }

    public function setPageId($pageId)
    {
        $this->_pageId = $pageId;

        return $this;
    }

    public function getPageId()
    {
        return $this->_pageId;
    }

    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setReplyId($replyId)
    {
        $this->_replyId = $replyId;

        return $this;
    }

    public function getReplyId()
    {
        return $this->_replyId;
    }

    public function setUserId($userId)
    {
        $this->_userId = $userId;

        return $this;
    }

    public function getUserId()
    {
        return $this->_userId;
    }

    public function setFullName($fullName)
    {
        $this->_fullName = $fullName;

        return $this;
    }

    public function getFullName()
    {
        return $this->_fullName;
    }

    public function setEmail($email)
    {
        $this->_email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setMessage($message)
    {
        $this->_message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function setPositive($positive)
    {
        $this->_positive = $positive;

        return $this;
    }

    public function getPositive()
    {
        return $this->_positive;
    }

    public function setNegative($negative)
    {
        $this->_negative = $negative;

        return $this;
    }

    public function getNegative()
    {
        return $this->_negative;
    }

    public function setDatePublished($datePublished)
    {
        $this->_datePublished = $datePublished;

        return $this;
    }

    public function getDatePublished()
    {
        return $this->_datePublished;
    }

    public function setRating($rating)
    {
        $this->_rating = $rating;

        return $this;
    }

    public function getRating()
    {
        return $this->_rating;
    }

    public function setIpAddress($ipAddress)
    {
        $this->_ipAddress = $ipAddress;

        return $this;
    }

    public function getIpAddress()
    {
        return $this->_ipAddress;
    }

    public function setPublished($published)
    {
        $this->_published = $published;

        return $this;
    }

    public function getPublished()
    {
        return $this->_published;
    }

    public function setSpam($spam)
    {
        $this->_spam = $spam;

        return $this;
    }

    public function getSpam()
    {
        return $this->_spam;
    }

    public function setSignUp($signUp)
    {
        $this->_signUp = $signUp;

        return $this;
    }

    public function getSignUp()
    {
        return $this->_signUp;
    }

    public function setCaptcha($captcha)
    {
        $this->_captcha = $captcha;

        return $this;
    }

    public function getCaptcha()
    {
        return $this->_captcha;
    }
}