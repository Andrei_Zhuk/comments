<?php

class Comments_Models_Configuration extends Application_Model_Models_Abstract
{
    protected $_id;
    protected $_listName;
    protected $_listType;
    protected $_pageId;
    protected $_uploader    = 0;
    protected $_aspects     = 0;
    protected $_noModerate  = 0;
    protected $_btnOnlyLogged = 0;
    protected $_onlyLogged  = 0;
    protected $_rating      = 0;
    protected $_avatars     = 0;
    protected $_limit       = 15;
    protected $_dateFormat  = 'MMMM dd, yyyy';
    protected $_orderInvert = 0;

    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setListName($listName)
    {
        $this->_listName = $listName;

        return $this;
    }

    public function getListName()
    {
        return $this->_listName;
    }

    public function setListType($listType)
    {
        $this->_listType = $listType;

        return $this;
    }

    public function getListType()
    {
        return $this->_listType;
    }

    public function setPageId($pageId)
    {
        $this->_pageId = $pageId;

        return $this;
    }

    public function getPageId()
    {
        return $this->_pageId;
    }

    public function setUploader($uploader)
    {
        $this->_uploader = $uploader;

        return $this;
    }

    public function getUploader()
    {
        return $this->_uploader;
    }

    public function setAspects($aspects)
    {
        $this->_aspects = $aspects;

        return $this;
    }

    public function getAspects()
    {
        return $this->_aspects;
    }

    public function setNoModerate($noModerate)
    {
        $this->_noModerate = $noModerate;

        return $this;
    }

    public function getNoModerate()
    {
        return $this->_noModerate;
    }

    public function setOnlyLogged($onlyLogged)
    {
        $this->_onlyLogged = $onlyLogged;

        return $this;
    }

    public function getOnlyLogged()
    {
        return $this->_onlyLogged;
    }

    public function setBtnOnlyLogged($btnOnlyLogged)
    {
        $this->_btnOnlyLogged = $btnOnlyLogged;

        return $this;
    }

    public function getBtnOnlyLogged()
    {
        return $this->_btnOnlyLogged;
    }

    public function setRating($rating)
    {
        $this->_rating = $rating;

        return $this;
    }

    public function getRating()
    {
        return $this->_rating;
    }

    public function setAvatars($avatars)
    {
        $this->_avatars = $avatars;

        return $this;
    }

    public function getAvatars()
    {
        return $this->_avatars;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;

        return $this;
    }

    public function getLimit()
    {
        return $this->_limit;
    }

    public function setDateFormat($dateFormat)
    {
        $this->_dateFormat = $dateFormat;

        return $this;
    }

    public function getDateFormat()
    {
        return $this->_dateFormat;
    }

    public function setOrderInvert($orderInvert)
    {
        $this->_orderInvert = $orderInvert;

        return $this;
    }

    public function getOrderInvert()
    {
        return $this->_orderInvert;
    }
}