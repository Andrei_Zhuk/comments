<?php

class Comments_Models_Rating extends Application_Model_Models_Abstract
{
    protected $_userId;
    protected $_value;
    protected $_commentId;

    public function setUserId($userId)
    {
        $this->_userId = $userId;

        return $this;
    }

    public function getUserId()
    {
        return $this->_userId;
    }

    public function setValue($value)
    {
        $this->_value = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function setCommentId($commentId)
    {
        $this->_commentId = $commentId;

        return $this;
    }

    public function getCommentId()
    {
        return $this->_commentId;
    }
}

