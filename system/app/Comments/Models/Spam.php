<?php

class Comments_Models_Spam extends Application_Model_Models_Abstract
{
    protected $_id;
    protected $_userId;
    protected $_message;

    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setUserId($userId)
    {
        $this->_userId = $userId;

        return $this;
    }

    public function getUserId()
    {
        return $this->_userId;
    }

    public function setMessage($message)
    {
        $this->_message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->_message;
    }
}

