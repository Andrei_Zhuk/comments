<?php

class Comments_DbTables_Configuration extends Zend_Db_Table_Abstract
{
    protected $_name = 'plugin_comments_configuration';

    public function selectConfig()
    {
        return $this->getAdapter()->fetchPairs($this->select());
    }
}
