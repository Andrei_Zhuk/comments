<?php

class Comments_Mappers_Comment extends Application_Model_Mappers_Abstract
{
    protected $_dbTable = 'Comments_DbTables_Comment';
    protected $_model   = 'Comments_Models_Comment';

    /**
     * @param $comment
     * @return mixed
     * @throws Exception
     * @throws Exceptions_SeotoasterException
     */
    public function save($comment)
    {
        if (!$comment instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }
        // TODO hook. need reorganize
        //$data = $comment->toArray();
        $data = array(
            'listName'      => $comment->getListName(),
            'listType'      => $comment->getListType(),
            'pageId'        => $comment->getPageId(),
            'id'            => $comment->getId(),
            'replyId'       => $comment->getReplyId(),
            'userId'        => $comment->getUserId(),
            'fullName'      => $comment->getFullName(),
            'email'         => $comment->getEmail(),
            'message'       => $comment->getMessage(),
            'positive'      => $comment->getPositive(),
            'negative'      => $comment->getNegative(),
            'datePublished' => $comment->getDatePublished(),
            'rating'        => $comment->getRating(),
            'ipAddress'     => $comment->getIpAddress(),
            'published'     => $comment->getPublished(),
            'spam'          => $comment->getSpam()
        );

        if ($comment->getId()) {
            return $this->getDbTable()->update($data, array('id = ?' => $comment->getId()));
        } else {
            return $this->getDbTable()->insert($data);
        }
    }

    /**
     *
     * @param $commentId
     * @return array|null
     * @throws Exception
     */
    public function findCommentsById($commentId)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $commentId);
        $where .= ' OR ' . $this->getDbTable()->getAdapter()->quoteInto('replyId = ?', $commentId);
        $select = $this->getDbTable()
                       ->select(Zend_Db_Table::SELECT_WITHOUT_FROM_PART)
                       ->from(array('l' => 'plugin_comments_list'))
                       ->where($where);
        $result = $this->getDbTable()->getAdapter()->fetchAll($select);

        return $result;
    }

    /**
     * @param      $listName
     * @param      $listType
     * @param      $pageId
     * @param bool $onlyPublished
     * @return null
     * @throws Exception
     */
    public function fetchMultiple($listName, $listType, $pageId, $spam, $onlyPublished = true)
    {
        // TODO need add ORDER BY
        $where = $this->getDbTable()->getAdapter()->quoteInto('listName = ?', $listName);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('listType = ?', $listType);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('spam = ?', $spam);
        if ($listType == 1) {
            $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('pageId = ?', $pageId);
        }
        if ($onlyPublished) {
            $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('published = ?', 1);
        }
        $select = $this->getDbTable()
                       ->select(Zend_Db_Table::SELECT_WITHOUT_FROM_PART)
                       ->from(array('l' => 'plugin_comments_list'))
                       ->joinLeft(array('u' => 'plugin_comments_user'), 'u.id = l.userId', array('imageName'))
                       ->setIntegrityCheck(false)
                       ->where($where);

        $resultSet = $this->getDbTable()->getAdapter()->fetchAll($select);
        if (null === $resultSet) {
            return null;
        }

        return $resultSet;
    }

    /**
     * @param $model
     * @return mixed
     * @throws Exceptions_SeotoasterException
     */
    public function delete($model)
    {
        if (!$model instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }
        $where[] = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $model->getId());

        return $this->getDbTable()->delete($where);
    }

    /**
     * @param $model
     * @return mixed
     * @throws Exception
     * @throws Exceptions_SeotoasterException
     */
    public function deleteSubComments($model)
    {
        if (!$model instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }
        $where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $model->getId());
        $where .= ' OR ' . $this->getDbTable()->getAdapter()->quoteInto('replyId = ?', $model->getId());

        return $this->getDbTable()->delete($where);
    }


//
//    public function fetchAll($where = null, $order = array()) {
//        $entries = array();
//        $resultSet = $this->getDbTable()->fetchAll($where, $order);
//        if(null === $resultSet) {
//            return null;
//        }
//        foreach ($resultSet as $row) {
//            $entries[] = new $this->_model($row->toArray());
//        }
//        return $entries;
//    }

//    public function fetchAll($where = '', $order = array())
//    {
//        $select = $this->getDbTable()->select();
//        if ($where) {
//            $select->where($where);
//        }
//        $optionsRowset = $this->getDbTable()->fetchAll($select);
//        return $optionsRowset->toArray();
//    }

    // For Api
    public function selectByParams($listName, $listType, $pageId)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto('listName = ?', $listName);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('listType = ?', $listType);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('spam = ?', 0);
        if ($listType == 1) {
            $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('pageId = ?', $pageId);
        }
        $select = $this->getDbTable()
                       ->select(Zend_Db_Table::SELECT_WITHOUT_FROM_PART)
                       ->from(array('l' => 'plugin_comments_list'))
                       ->joinLeft(array('u' => 'plugin_comments_user'), 'u.id = l.userId', array('imageName'))
                       ->setIntegrityCheck(false)
                       ->where($where);

        $resultSet = $this->getDbTable()->getAdapter()->fetchAll($select);
        if (null === $resultSet) {
            return null;
        }

        return $resultSet;
    }



    /**
     * ==/ For API /==
     */

    public function getComments($name, $type, $pageId)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto('listName = ?', $name);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('listType = ?', $type);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('spam = ?', 0);
        if ($type == 1) {
            $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('pageId = ?', $pageId);
        }

        $select = $this->_dbTable->getAdapter()->fetchAll(
            $this->_dbTable->getAdapter()->select()
                           ->from(array('l' => 'plugin_comments_list'))
                           ->joinLeft(array('u' => 'plugin_comments_user'), 'u.id = l.userId', array('imageName'))
                           ->where($where)
        );

        if (null === $select) {
            return null;
        }
        return $select;
    }

    public function getComment($commentId) {
        $where = $this->getDbTable()->getAdapter()->quoteInto("id = ?", $commentId);
        return $this->getDbTable()->getAdapter()->fetchAll(
            $this->getDbTable()->getAdapter()->select()
                           ->from(array('l' => 'plugin_comments_list'))
                           ->where($where)
        );
    }

}