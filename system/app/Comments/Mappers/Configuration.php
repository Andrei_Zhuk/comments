<?php

class Comments_Mappers_Configuration extends Application_Model_Mappers_Abstract
{
    protected $_dbTable = 'Comments_DbTables_Configuration';
    protected $_model   = 'Comments_Models_Configuration';

    /**
     * @param $model
     * @return mixed
     * @throws Exception
     * @throws Exceptions_SeotoasterException
     */
    public function save($model)
    {
        if (!$model instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }

        $data = $model->toArray();

        if(!$model->getId()){
            $result = $this->getDbTable()->insert($data);
            return $result;
        } else {
            $result = $this->getDbTable()->update($data, array('id = ?' => $model->getId()));
            return $result;
        }
    }

    /**
     * @param $name
     * @param $type
     * @param $pageId
     * @return mixed
     * @throws Exception
     */
    public function findOneConfigs($name, $type, $pageId)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto('listName = ?', $name);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('listType = ?', $type);
        if ($type == 1) {
            $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('pageId = ?', $pageId);
        }

        $row = $this->getDbTable()->fetchAll($where)->current();
        if (null == $row) {
            return null;
        }
        return $row->toArray();
    }


    public function findByName($name)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto("listName = ?", $name);
        $row = $this->getDbTable()->fetchAll($where)->current();
        if (null == $row) {
            return null;
        }
        return $row->toArray();
    }

    /**
     * @param string $where
     * @param array $order
     * @return array|null
     * @throws Exception
     */
    public function fetchAll($where = '', $order = array())
    {
        $select = $this->getDbTable()->select();
        if ($where) {
            $select->where($where);
        }
        $optionsRowset = $this->getDbTable()->fetchAll($select);
        return $optionsRowset->toArray();
    }
}