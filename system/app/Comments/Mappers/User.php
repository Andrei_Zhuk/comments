<?php


class Comments_Mappers_User extends Application_Model_Mappers_Abstract
{
    protected $_dbTable = 'Comments_DbTables_User';
    protected $_model = 'Comments_Models_User';

    /**
     * @param $credo
     * @return mixed
     * @throws Exception
     * @throws Exceptions_SeotoasterException
     */
    public function save($credo)
    {
        if (!$credo instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }
        $data = array(
            'id'        => $credo->getId(),
            'imageName' => $credo->getImageName()
        );

        if ($this->find(array( 'id' => $credo->getId() )) !== null) {
            return $this->getDbTable()->update($data, array( 'id = ?' => $credo->getId() ));
        } else {
            return $this->getDbTable()->insert($data);
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        /*if (!$data instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }
        $data = array(
            'id'        => $data->getId(),
        );*/
        $where[] = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $id);

        return $this->getDbTable()->delete($where);
    }
}