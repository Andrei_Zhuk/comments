<?php

class Comments_Mappers_Spam extends Application_Model_Mappers_Abstract
{
    protected $_dbTable = 'Comments_DbTables_Spam';
    protected $_model   = 'Comments_Models_Spam';

    /**
     * @param $model
     * @return mixed
     * @throws Exception
     * @throws Exceptions_SeotoasterException
     */
    public function save($model)
    {
        if (!$model instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }

        $data = $model->toArray();

        return $this->getDbTable()->insert($data);
    }

    /**
     * @param $message
     * @return null
     * @throws Exception
     */
    public function findByMessage($message) {
        $where = $this->getDbTable()->getAdapter()->quoteInto("message = ?", $message);
        $row   = $this->getDbTable()->fetchAll($where)->current();
        if(!$row) {
            return null;
        }
        return new $this->_model($row->toArray());
    }
}