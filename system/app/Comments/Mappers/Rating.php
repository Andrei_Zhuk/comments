<?php

class Comments_Mappers_Rating extends Application_Model_Mappers_Abstract
{
    protected $_dbTable = 'Comments_DbTables_Rating';
    protected $_model   = 'Comments_Models_Rating';

    /**
     * @param $model
     * @return mixed
     * @throws Exception
     * @throws Exceptions_SeotoasterException
     */
    public function save($model)
    {
        if (!$model instanceof $this->_model) {
            throw new Exceptions_SeotoasterException('Given parameter should be ' . $this->_model . ' instance');
        }

        $data = $model->toArray();

        return $this->getDbTable()->insert($data);
    }

    /**
     * @param $userId
     * @param $commentId
     * @return mixed
     * @throws Exception
     */
    public function getRatingPoint($userId, $commentId)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto('userId = ?', $userId);
        $where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('commentId = ?', $commentId);
        $select = $this->getDbTable()->getAdapter()->select()
                       ->from(array( 'rating' => 'plugin_comments_rating' ))
                       ->where($where);

        $result = $this->getDbTable()->getAdapter()->fetchAll($select);

        return $result;
    }


    public function fetchAll($where = '', $order = array())
    {
        $select = $this->getDbTable()->select();
        if ($where) {
            $select->where($where);
        }
        $optionsRowset = $this->getDbTable()->fetchAll($select);
        return $optionsRowset->toArray();
    }

    public function fetchByUserIdOnPage($userId, $pageId = null)
    {
        $where = $this->getDbTable()->getAdapter()->quoteInto('userId = ?', $userId);
        //$where .= ' AND ' . $this->getDbTable()->getAdapter()->quoteInto('pageId = ?', $pageId);;
        $optionsRowset = $this->getDbTable()->fetchAll($where);
        return $optionsRowset->toArray();
    }
}