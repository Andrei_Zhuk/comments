<?php

class Comments_MailWatchdog implements Interfaces_Observer
{
    const TRIGGER_NEW_COMMENT = 'c_newcomment';
    const TRIGGER_NEW_REPLY = 'c_newreply';

    /**
     *
     * @param array $options
     * @throws Zend_Controller_Action_Exception
     */
    public function __construct($options = array())
    {
        $this->_entityParser = new Tools_Content_EntityParser();
        $this->_mailer = Tools_Mail_Tools::initMailer();
        $this->_options = $options;
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_translator = Zend_Controller_Action_HelperBroker::getStaticHelper('language');
        $this->_configHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('config');
    }

    /**
     *
     * @param $object
     */
    public function notify($object)
    {
        if (!$object) {
            return false;
        }
        if (isset($this->_options['trigger'])) {
            $methodName = '_send' . str_replace(array( ' ', '_' ), '', ucwords($this->_options['trigger'])) . 'Mail';
            if (method_exists($this, $methodName)) {
                return $this->$methodName($object);
            }
        }
    }

    /**
     *
     *
     * @param Comments_Models_Comment $comment
     * @return bool
     * @throws Exceptions_SeotoasterException
     */
    private function _sendCnewcommentMail(Comments_Models_Comment $comment)
    {
        $userMapper = Application_Model_Mappers_UserMapper::getInstance();
        $adminEmail = isset($systemConfig['adminEmail']) ? $systemConfig['adminEmail'] : 'admin@localhost';

        switch ($this->_options['recipient']) {
            case Tools_Security_Acl::ROLE_SUPERADMIN:
                $users = $userMapper->getDbTable()->fetchAll(array( 'role_id = ?' => Tools_Security_Acl::ROLE_SUPERADMIN ));
                if ($users->count()) {
                    foreach ($users as $user) {
                        $this->_mailer->setMailTo($user['email']);
                    }
                } else {
                    return false;
                }
                break;
            case Tools_Security_Acl::ROLE_ADMIN:
                $this->_mailer->setMailToLabel('Admin')->setMailTo($adminEmail);
                $where = $userMapper->getDbTable()->getAdapter()->quoteInto(
                    "role_id = ?",
                    Tools_Security_Acl::ROLE_ADMIN
                );
                $adminUsers = $userMapper->fetchAll($where);
                if (!empty($adminUsers)) {
                    $adminBccArray = array();
                    foreach ($adminUsers as $admin) {
                        array_push($adminBccArray, $admin->getEmail());
                    }
                    if (!empty($adminBccArray)) {
                        $this->_mailer->setMailBcc($adminBccArray);
                    }
                }
                break;
            case Tools_Security_Acl::ROLE_MEMBER:
                $commentMapper = Comments_Mappers_Comment::getInstance();
                $email = $comment->getEmail();
                $this->_mailer->setMailToLabel('Admin')->setMailTo($email);
                break;
            case Tools_Security_Acl::ROLE_GUEST:
                $commentMapper = Comments_Mappers_Comment::getInstance();
                $email = $comment->getEmail();
                $this->_mailer->setMailToLabel('Admin')->setMailTo($email);
                break;
            default:
                error_log('Unsupported recipient ' . $this->_options['recipient'] . ' given');

                return false;
                break;
        }
        if (($mailBody = $this->_prepareEmailBody()) == false) {
            $mailBody = $this->_options['message'];
        }

        $this->_entityParser->objectToDictionary($comment);

        $page = Application_Model_Mappers_PageMapper::getInstance()->find($comment->getPageId());
        if ($page instanceof Application_Model_Models_Page) {
            $reviewPage = '<a title="page review" href="' . $this->_websiteHelper->getUrl() . $page->getUrl() . '">' . $this->_websiteHelper->getUrl() . $page->getUrl() . '</a>';
            $this->_entityParser->addToDictionary(array( 'comment:pageurl' => $reviewPage, 'comment:authorname' => $comment->getFullName() ));
        }

        $this->_mailer
            ->setBody($this->_entityParser->parse($mailBody))
            ->setSubject($this->_options['subject'])
            ->setMailFrom($this->_options['from'])
            ->setMailFromLabel($this->_options['from']);

        return ($this->_mailer->send() !== false);
    }

    /**
     *
     *
     * @param Comments_Models_Comment $comment
     * @return bool
     * @throws Exceptions_SeotoasterException
     */
    private function _sendCnewreplyMail(Comments_Models_Comment $comment)
    {
        $userMapper = Application_Model_Mappers_UserMapper::getInstance();
        $adminEmail = isset($systemConfig['adminEmail']) ? $systemConfig['adminEmail'] : 'admin@localhost';

        switch ($this->_options['recipient']) {
            case Tools_Security_Acl::ROLE_SUPERADMIN:
                $users = $userMapper->getDbTable()->fetchAll(array( 'role_id = ?' => Tools_Security_Acl::ROLE_SUPERADMIN ));
                if ($users->count()) {
                    foreach ($users as $user) {
                        $this->_mailer->setMailTo($user['email']);
                    }
                } else {
                    return false;
                }
                break;
            case Tools_Security_Acl::ROLE_ADMIN:
                $this->_mailer->setMailToLabel('Admin')->setMailTo($adminEmail);
                $where = $userMapper->getDbTable()->getAdapter()->quoteInto(
                    "role_id = ?",
                    Tools_Security_Acl::ROLE_ADMIN
                );
                $adminUsers = $userMapper->fetchAll($where);
                if (!empty($adminUsers)) {
                    $adminBccArray = array();
                    foreach ($adminUsers as $admin) {
                        array_push($adminBccArray, $admin->getEmail());
                    }
                    if (!empty($adminBccArray)) {
                        $this->_mailer->setMailBcc($adminBccArray);
                    }
                }
                break;
            case Tools_Security_Acl::ROLE_MEMBER:
                $commentMapper = Comments_Mappers_Comment::getInstance();
                $email = $comment->getEmail();
                $this->_mailer->setMailToLabel('Admin')->setMailTo($email);
                break;
            case Tools_Security_Acl::ROLE_GUEST:
                $commentMapper = Comments_Mappers_Comment::getInstance();
                $email = $comment->getEmail();
                $this->_mailer->setMailToLabel('Admin')->setMailTo($email);
                break;
            default:
                error_log('Unsupported recipient ' . $this->_options['recipient'] . ' given');

                return false;
                break;
        }
        if (($mailBody = $this->_prepareEmailBody()) == false) {
            $mailBody = $this->_options['message'];
        }

        $this->_entityParser->objectToDictionary($comment);

        $page = Application_Model_Mappers_PageMapper::getInstance()->find($comment->getPageId());
        if ($page instanceof Application_Model_Models_Page) {
            $reviewPage = '<a title="page review" href="' . $this->_websiteHelper->getUrl() . $page->getUrl() . '">' . $this->_websiteHelper->getUrl() . $page->getUrl() . '</a>';
            $this->_entityParser->addToDictionary(array( 'comment:pageurl' => $reviewPage, 'comment:authorname' => $comment->getFullName() ));
        }

        $this->_mailer
            ->setBody($this->_entityParser->parse($mailBody))
            ->setSubject($this->_options['subject'])
            ->setMailFrom($this->_options['from'])
            ->setMailFromLabel($this->_options['from']);

        return ($this->_mailer->send() !== false);
    }

    /**
     *
     * @return bool|mixed
     * @throws Zend_Exception
     */
    protected function _prepareEmailBody()
    {
        $tmplMessage = $this->_options['message'];
        $mailTemplate = Application_Model_Mappers_TemplateMapper::getInstance()->find($this->_options['template']);

        if (!empty($mailTemplate)) {
            $this->_entityParser->setDictionary(array(
                'emailmessage' => !empty($tmplMessage) ? $tmplMessage : ''
            ));
            //pushing message template to email template and cleaning dictionary
            $mailTemplate = $this->_entityParser->parse($mailTemplate->getContent());
            $this->_entityParser->setDictionary(array());
            $mailTemplate = $this->_entityParser->parse($mailTemplate);

            $themeData = Zend_Registry::get('theme');
            $extConfig = Zend_Registry::get('extConfig');
            $parserOptions = array(
                'websiteUrl'   => $this->_websiteHelper->getUrl(),
                'websitePath'  => $this->_websiteHelper->getPath(),
                'currentTheme' => $extConfig['currentTheme'],
                'themePath'    => Tools_Filesystem_Tools::cleanWinPath($themeData['path']),
            );

            $page = Application_Model_Mappers_PageMapper::getInstance()->findByUrl('index.html');
            if ($page instanceof Application_Model_Models_Page) {
                $parser = new Tools_Content_Parser($mailTemplate, $page->toArray(), $parserOptions);

                return Tools_Content_Tools::stripEditLinks($parser->parseSimple());
            }
        }

        return false;
    }
}
